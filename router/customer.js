const express = require('express')
const router = express.Router()
const { list, create, update, destroy } = require('../controllers/customerController')

router.get('/list', list) // route untuk endpoint list
router.post('/create', create) // route untuk endpoint create
router.put('/update', update) // route untuk endpoint update
router.delete('/destroy', destroy) // route untuk endpoint destroy

module.exports = router