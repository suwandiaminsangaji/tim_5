const express = require('express')
const router = express.Router()
const customerRouter = require('./customer') //inisiasi router customer

router.get('/check', (req, res) => res.send("Application Up"))
router.use('/customer', customerRouter) // implementasi route category dengan /customer


module.exports = router